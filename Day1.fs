﻿module Day1

type Direction = North | South | East | West

type Position = { x: int; y: int }

type Walker = { facing: Direction; location: Position; history: Position list}

let rotateRight (d:Direction) = 
    match d with
    | North -> East
    | East -> South
    | South -> West
    | West -> North

let rotateLeft (d:Direction) = d |> rotateRight |> rotateRight |> rotateRight

let changeY n (l:Position) = {l with y = l.y + n }
let changeX n (l:Position) = {l with x = l.x + n }

let track (w:Walker) = { w with history = w.history @ [w.location] }

let step (w:Walker) = 
    match w.facing with
    | North -> { w with location = changeY 1 w.location }
    | East -> { w with location = changeX 1 w.location }
    | South -> { w with location = changeY -1 w.location }
    | West -> { w with location = changeX -1 w.location }

let rec move n (w:Walker) =     
     if n = 0 then w
     else move (n-1) (step w |> track)  
    
let L n (w:Walker) = move n {w with facing = rotateLeft w.facing} 
let R n (w:Walker) = move n {w with facing = rotateRight w.facing}

let rec firstRepeat' (been : Position list)  (positions : Position list) : Position option = 
        let head = positions.Head
        if (List.isEmpty positions) then None
        elif (List.exists ((=) head) been) then Some head
        else firstRepeat' (been @ [head]) positions.Tail

let firstRepeat (l : Position list) = firstRepeat' [] l 

let distanceFrom (p : Position) = abs p.x + abs p.y   

let day1 = 
    printfn "Day 1\n"
    let start = { facing=North; location = { x=0; y=0 }; history = [] }    
    let finish = start |> (R 3) |> (L 2) |> (L 2) |> (R 4) |> (L 1) |> (R 2) |> (R 3) |> (R 4) |> (L 2) |> (R 4) |> (L 2) |> (L 5) |> (L 1) |> (R 5) |> (R 2) |> (R 2) |> (L 1) |> (R 4) |> (R 1) |> (L 5) |> (L 3) |> (R 4) |> (R 3) |> (R 1) |> (L 1) |> (L 5) |> (L 4) |> (L 2) |> (R 5) |> (L 3) |> (L 4) |> (R 3) |> (R 1) |> (L 3) |> (R 1) |> (L 3) |> (R 3) |> (L 4) |> (R 2) |> (R 5) |> (L 190) |> (R 2) |> (L 3) |> (R 47) |> (R 4) |> (L 3) |> (R 78) |> (L 1) |> (R 3) |> (R 190) |> (R 4) |> (L 3) |> (R 4) |> (R 2) |> (R 5) |> (R 3) |> (R 4) |> (R 3) |> (L 1) |> (L 4) |> (R 3) |> (L 4) |> (R 1) |> (L 4) |> (L 5) |> (R 3) |> (L 3) |> (L 4) |> (R 1) |> (R 2) |> (L 4) |> (L 3) |> (R 3) |> (R 3) |> (L 2) |> (L 5) |> (R 1) |> (L 4) |> (L 1) |> (R 5) |> (L 5) |> (R 1) |> (R 5) |> (L 4) |> (R 2) |> (L 2) |> (R 1) |> (L 5) |> (L 4) |> (R 4) |> (R 4) |> (R 3) |> (R 2) |> (R 3) |> (L 1) |> (R 4) |> (R 5) |> (L 2) |> (L 5) |> (L 4) |> (L 1) |> (R 4) |> (L 4) |> (R 4) |> (L 4) |> (R 1) |> (R 5) |> (L 1) |> (R 1) |> (L 5) |> (R 5) |> (R 1) |> (R 1) |> (L 3) |> (L 1) |> (R 4) |> (L 1) |> (L 4) |> (L 4) |> (L 3) |> (R 1) |> (R 4) |> (R 1) |> (R 1) |> (R 2) |> (L 5) |> (L 2) |> (R 4) |> (L 1) |> (R 3) |> (L 5) |> (L 2) |> (R 5) |> (L 4) |> (R 5) |> (L 5) |> (R 3) |> (R 4) |> (L 3) |> (L 3) |> (L 2) |> (R 2) |> (L 5) |> (L 5) |> (R 3) |> (R 4) |> (R 3) |> (R 4) |> (R 3) |> (R 1)    
        
    printfn "Part 1: %d" (distanceFrom finish.location)
    match (firstRepeat finish.history) with
    | Some p -> printfn "Part 2: %d" (distanceFrom p)
    | None -> printfn "Part 2: Couldn't find it"
